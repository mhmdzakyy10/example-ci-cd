const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.json());
const cors = require("cors");
app.use(cors());

app.listen(process.env.PORT || 3000, () => {
  console.log("connected");
});

app.get("/", (req, res) => {
  res.status(200).json({
    success: true,
  });
});
